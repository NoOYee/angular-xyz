import { Injectable }    from '@angular/core';  
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class JsonDataService {
  constructor(private http: Http) {}
  
  // Returns list of animals based input parameters.  
  public getJson(url : string) {  
    // Returns response  
    return this.http.get(url)
        .map(response => response.json());
  }
  
}