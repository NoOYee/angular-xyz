import { Injectable } from '@angular/core';
import { CacheService } from 'ng2-cache-service';

@Injectable()
export class NgCacheService {

  constructor(private _cacheService: CacheService) {}

  public setCache(key,set_data) {
    //set global prefix as build version
    this._cacheService.set(key, set_data, {tag: key});
  }

  public getCache(key){
    return this._cacheService.get(key);
  }

  public removeCacheTag(tag){
    this._cacheService.removeTag(tag);
    return true;
  }

  public removeCacheAll(){
    this._cacheService.removeAll();
    return false;
  }
}
