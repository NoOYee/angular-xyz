import {Component, OnInit } from '@angular/core';
import {JsonDataService} from '../../services/json-data.service'

@Component({
  selector: 'app-tab-c',
  templateUrl: './tab-c.component.html',
  styleUrls: ['./tab-c.component.css'],
  providers: [JsonDataService]
})
export class TabCComponent implements OnInit {
  private jsonData;
  constructor(private jsonService : JsonDataService) { }

  ngOnInit() {
    this.getJson();
  }

  public getJson(){
    //let url = 'http://localhost/data/MOCK_DATA_2.json';
    let url = 'assets/MOCK_DATA_2.json';
    this.jsonService.getJson(url).subscribe(data => {
      this.jsonData = data;
    });
  }
}
