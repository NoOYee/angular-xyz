import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { routing } from './app.routing';
import { AppComponent } from './app.component';
import { TopmenuComponent } from './components/topmenu/topmenu.component';
import { HomeComponent } from './components/home/home.component';
import { TabAComponent } from './components/tab-a/tab-a.component';
//import { TabBComponent } from './components/tab-b/tab-b.component';
import { TabCComponent } from './components/tab-c/tab-c.component';
//import { TabDComponent } from './components/tab-d/tab-d.component';



@NgModule({
  declarations: [
   	AppComponent,
    TopmenuComponent,
    TabAComponent,
    HomeComponent,
    TabCComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    HttpModule,
    RouterModule,
    routing,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
